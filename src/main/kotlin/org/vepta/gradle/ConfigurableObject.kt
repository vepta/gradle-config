package org.vepta.gradle

import groovy.lang.Closure
import org.gradle.util.Configurable

abstract class ConfigurableObject<T> : Configurable<T> {
    override fun configure(cl: Closure<Any>): T {
        cl.delegate = this
        cl.call()
        return this as T
    }
}