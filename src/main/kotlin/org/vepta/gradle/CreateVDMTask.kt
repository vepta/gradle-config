package org.vepta.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.get

class CreateVDMTask : DefaultTask() {
    /**
     * Title of module, must be entirely in lowercase
     * and follow standard Java package naming rules
     */
    var title: String? = null

    /**
     * The package name of the module, should follow
     * standard Java package naming rules
     */
    var packageName: String? = null

    /**
     * The version of the module, must be strictly
     * incrementing across module versions
     */
    var version: Long? = null

    /**
     * The class that controls the module, must have
     * a no-args primary constructor
     */
    var moduleClass: String? = null

    /**
     * A list of modules that this module requires to load
     *
     * You may omit the package name of the module if it is
     * in the same package as this module
     */
    var depends: List<String> = mutableListOf()

    /**
     * A list of modules that this module requires to load
     *
     * You may omit the package name of the module if it is
     * in the same package as this module
     *
     * You do not have to include the name of this module, it will
     * be automatically added
     */
    var provides: List<String> = mutableListOf()

    /**
     * A list of modules that this module conflicts with
     *
     * You may omit the package name of the module if it is
     * in the same package as this module
     */
    var conflicts: List<String> = mutableListOf()

    @TaskAction
    fun createModule() {
        val folders = project.configurations["compile"].map {
        }
    }
}