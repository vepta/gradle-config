plugins {
    `kotlin-dsl`
}

/*java {
    sourceSets {
        getByName("main").java.srcDir(relativeProjectPath("../modules/core/dynload/src/main/sharedio"))
    }
}*/

repositories {
    mavenCentral()
    maven { setUrl("https://jitpack.io") }
    jcenter()
    maven { setUrl("https://kotlin.bintray.com/kotlinx") }
}

dependencies {
    implementation(group = "org.tukaani", name = "xz", version = "1.8")
}
