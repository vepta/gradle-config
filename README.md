# **VEPTA:** gradle-config

Shared Gradle configurations/plugins for the entire VEPTA organization.

This configuration will:

- Configure Java + Kotlin
- Enable coroutines and include the coroutines support library
- Import the shared dependencies (logging, Kodein DI, testing, Kotlin reflection)
- Allow building the current project as a VDM

## Configuring your build
In `build.gradle`:
```groovy
buildscript {
    apply from: "https://build.vepta.org/gradle/buildscript.gradle", to: buildscript
}

apply from: "https://build.vepta.org/gradle/core.gradle"
```

In `gradle.properties`:
```
# VEPTA recommended
org.gradle.parallel=true
org.gradle.caching=true
kotlin.parallel.tasks.in.project=true
```

In `settings.gradle`
```
rootProject.name = "{YOUR PROJECT NAME}"
```

Download the recommended `.gitignore`:
```
wget "https://gitlab.com/vepta/gradle-config/raw/develop/.gitignore"
```

Run:
```
git init
gradle build
gradle build
gradle wrapper
```

We run `gradle build` twice as we expect the first build to fail while the script downloads the Gradle plugin. All further build attempts should succeed.
